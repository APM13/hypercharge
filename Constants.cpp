//Constants and global defintions.
namespace Constants
{
//Global variables
extern float CollisionFrequency=1;// 1/s 
extern float Efieldx=5;// V/cm
extern int NElectrons = 1000; 
extern float DriftDistance=20; //cm
extern float DriftTime=0; // s
extern float DriftVelocity=0;
extern float ElectronCharge=1;
extern float ElectronMass=1;
extern float AtomicMass=1000;
extern float Accelerationx=Efieldx*ElectronCharge/ElectronMass;
extern float Norm=0.0;
}
#define PI 3.14159265358979323846
