/* HYbrid PERovskite Charge Transport Simulator (HyPerCharge).
 * Andrew P. McMahon - Imperial College London.
 * Began 26/4/2015.
 *
 * */

/* Basic flow of the program:
 * from - http://journals.aps.org/rmp/pdf/10.1103/RevModPhys.55.645
 * Definition of system/physical param's.
 * 	|
 * 	|
 * 	V
 * Initial conditions of motion
 *	|
 *	|
 *	V
 * Stochastic determination of flight duration
 * ^	|	
 * |	|
 * |	V
 * Determination of electron state before scattering
 * |	|
 * |	|
 * |	V
 * Collection of data for estimators
 * |		     |
 * |		     |
 * |		     V
 * |	           ------
 * |	 NO  <--   | t<T |--> YES --> Final evaluation of 
 * |	 |          ------            estimators.
 * |	 |				|
 * |	 V				V
 * Stochastic determination	     Print result
 * of scattering mechanism		|
 * |	|				|
 * |	|				V
 * |	V			STOP	
 * Stochastic determination
 * of electron state just after 
 * scattering.	 
 */
//Includes for the main program
#include<iostream>
#include<fstream>
#include "FlightFuncs.h"
//#include "Constants.h"
#include<cstdlib>
#include<ctime>

//First implementation of program from
//http://uigelz.eecs.umich.edu/pub/short_courses/MCSHORT_0502.pdf
using namespace std;

float CollisionFrequency=1e9;//1;// 1/s 
//float Efieldx=5;// V/cm
int Efieldx=1; //V/cm	 
int NElectrons = int(1e5); //int(1e6);//1000;//int(1e15);
float DriftDistance=20; //cm 
float DriftTime=0; // s
float DriftVelocity=0;
float ElectronCharge=1.6e-12;//not sure units here from presentation...//1;
float ElectronMass=0.911e-27;//1;
float AtomicMass=20.0*1.67e-24;//g //1000;
float Accelerationx=Efieldx*ElectronCharge/ElectronMass;
float Norm=0.0;
int ScatterCounter=0;
int PropagateCalls=0;

int main() { 	
	
	/*initialise rand()*/
	//srand(time(NULL));
	srand(1234);
	
	/* OUTPUT */
	ofstream dataStream;
	dataStream.open("Data.dat");
	dataStream << "#HyPerCharge - simulation began at "<<time(0) << "\n";
    dataStream << "#E Vdrift_final\n";
	for(Efieldx=5;Efieldx<10;Efieldx++) {
        std::cout << "Running at " << Efieldx << " V/m." << std::endl;
    DriftVelocity=0.0;
	DriftTime=0.0;
	Norm=0.0;
	/*Declare and initialise electron e1*/
	Electron e1;
	e1.vx=0,e1.vy=0,e1.vz=0,e1.k=0,e1.E=0,e1.x=0;
	//Perform device propagation for NEelectron ensemble.
	for(int i=0;i<NElectrons;i++) {
    if(i%(NElectrons/10)==0) {
            std::cout << "Another 10% of electrons propagated..." << std::endl;
    }
    //e1.vx=0,e1.vy=0,e1.vz=0,e1.k=0,e1.E=0,e1.x=0;
	Propagate(&e1);
	}
	
	/* OUTPUT */
	//cout << "Vdrift = " << DriftVelocity/Norm << endl;
	//cout << "Final e velocity = ("<<e1.vx<<", "<<e1.vy<<", "<<e1.vz<<")"<<endl;
	dataStream << Efieldx << " " << DriftVelocity/Norm << "\n";
	}
    dataStream << "#HyPerCharge - simulation ended at "<<time(0)<< "\n";
	dataStream.close();
	return 0;
}	


