/* Flight functions for HyPerCharge 
 * Started 4/5/2015 
 * */
#include "FlightFuncs.h"
#include<cstdlib>
#include<cmath>
#include "Constants.h"
#include<iostream>

extern float CollisionFrequency;// 1/s 
//extern float Efieldx;// V/cm
extern int Efieldx;
extern int NElectrons;
extern float DriftDistance; //cm
extern float DriftTime; // s
extern float DriftVelocity;
extern float ElectronCharge;
extern float ElectronMass;
extern float AtomicMass;
extern float Accelerationx;
extern float Norm;
extern int ScatterCounter;
extern int PropagateCalls;

using namespace std;


//Rotate vector x,y,z with magnitude r by angles alpha (azimuth),beta(polar),theta,phi.
/* Collision dynamics, see pg 12,13 of http://uigelz.eecs.umich.edu/pub/short_courses/MCSHORT_0502.pdf.
Idea is the following:
1) Determine Euler angles (alpha,beta) -> (polar, azimuth) corresponding to initial electron velocity, \vec{v}_i.
2) Rotate frame by (alpha,beta) so that z,x axes are aligned with \vec{v}_i and v_i,x respectively.
3) Rotate \vec{v}_i by (theta,phi) to get direction of \vec{v}_final.
4) Account for change in speed |\vec{v}_final|=|\vec{v}_i|-2\deltaE/m_e.
5) Rotate frame by (-alpha,-beta) to orignal orientation.

End result is scattering matrix M, \vec{v}_final=|\vec{v}_final|*M ->Implemented in scatter.
*/
void Scatter(float* vx,float* vy, float* vz,float v,float alpha,float beta,float theta,float phi) {
	//cout << "r = " << *x << " " << *y << " " << *z << " - r " << r << endl;	
	*vx=v*(cos(beta)*cos(alpha)*sin(theta)*cos(phi)+cos(beta)*sin(alpha)*cos(theta)-sin(beta)*sin(theta)*sin(phi));
	*vy=v*(sin(beta)*cos(alpha)*sin(theta)*cos(phi)+sin(beta)*sin(alpha)*cos(theta)+cos(beta)*sin(theta)*sin(phi));
	*vz=v*(-sin(alpha)*sin(theta)*cos(phi)+cos(alpha)*cos(theta));
	//cout << "r = " << *x << " " << *y << " " << *z << " - r " << r << endl;
}

void Propagate(Electron* e) {
	PropagateCalls++;
	//randomly choose time to next collision
	float dt;
	
	do {
	//Generate random number between 0 and 1;
	float RANDOM=1.0*rand()/RAND_MAX;
	//constant collision freq, random time between collisions is dt:
	//1.0000...1 is to stop logf freaking out with zero arg.
	dt=-(1.0/CollisionFrequency)*logf(1.000000001-RANDOM);
	
	//Update position & speed of particle
	e->x = e->x+e->vx*dt+0.5*Accelerationx*dt*dt;
	e->vx = e->vx+Accelerationx*dt;
	DriftTime=DriftTime+dt;

	//If electron hasn't reached drift dist, scatter
	//if(e->x<DriftDistance) {
		ScatterCounter++;
		//Magnitude of electron velocity
		float v=sqrt(e->vx*e->vx+e->vy*e->vy+e->vz*e->vz);
		//Eulerian angles for initial velocity
		float beta=atan2(e->vy,e->vx);
		float alpha=acosf(e->vz/v);
		//Randomly choose scaterring angles
		float theta=acosf(2*(1.0*rand()/RAND_MAX)-1.0);
		float phi=2*PI;
		//Account for energy lost during elastic collision
		v=v*sqrt(1.0-(2.0*ElectronMass/AtomicMass)*(1-cos(theta)));
		//Perform the scatter, use dummy variables vx,vy,vz for now...
		float vx=e->vx,vy=e->vy,vz=e->vz;
		Scatter(&vx,&vy,&vz,v,alpha,beta,theta,phi);
		e->vx=vx,e->vy=vy,e->vz=vz;
	
	//}
	}while(e->x<DriftDistance);
	//else if(e->x>=DriftDistance){
	DriftVelocity=DriftVelocity+(e->x)/float(DriftTime);
	if(DriftVelocity!=DriftVelocity && PropagateCalls%(int(100000))==0) {
	cout << "\n\n Error!\n\n" <<endl;
	cout << "ex = " << e->x << endl;
	cout << "DriftTime = " << DriftTime << endl;
	} 
	Norm+=1;
	if(PropagateCalls%(int(100000))==0) {
	cout << "Reached end of device" << endl;
	cout << "Norm = " << Norm << endl;
	cout << "DriftVelocity = " << DriftVelocity << endl;
	cout << "DriftTime = " << DriftTime << endl;
	cout << ScatterCounter << " scattering events ..." << endl;
	}
	e->vx=0;e->vy=0;e->vz=0;e->x=0;
	//}
	//else {
	//cout << "Error, electron has left this plane of existence" << endl;
	//}
}
