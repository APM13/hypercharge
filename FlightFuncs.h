#ifndef FlightFuncs_H
#define FlightFuncs_H

class Electron {
	public:
	//wavevector/momentum
	float k;
	//energy
	float E;
	//velocity
	float vx,vy,vz;
	//position
	float x;
};
void Propagate(Electron*);
void Scatter(float*,float*, float*,float,float,float,float,float);
#endif
